# Overview and Setup

Duration: 60 minutes

## Talk

(10 minutes)

Sometimes, it's worth the administrative burden of supporting your own server of a thing, in order to gain some advantages of security (privacy) or scalability, or whatever.

In SFS' case, we chose to run our own GitLab CE omnibus, in order to get the integrated Mattermost.

## Demo

(15 minutes)

- Start from ??? CentOS 7.x ???
- Install GitLab CE omnibus
- Verify access to GitLab and Mattermost

## Pair & Share

(30 minutes)

- Start with a ??? CentOS 7 ??? machine that has nothing more important to do.

Raise both hands in the air and make a victorious noise: Woo! or similar.

Teacher: When most pairs have finished, if there's time, lead a group Share.
TODO:
- finish normal instructions
- add how to enable Mattermost
- add how to SSL (offload/letsencrypt)
- add how to enable Repository
- add Docker Hub stuph: gitlab/gitlab-{ce,ee,runner}
