# Pie Crust

## Ingredients

* 2C Flour
* 3/4C + 2T COLD salted butter (cubed)  
* 2T butter for filling
* 5T Ice Water

Preheat oven to 450 degrees

1. Blend flour and butter with a pastry blender until you achieve pea sized cubes of butter
2. Slowly add the ice water to the flour/butter mixture until incorporated
3. For best texture split dough into two halves, wrap with saran wrap and chill for 30 minutes
4. Pull from fridge.  Dust the surface with flour.  Roll out with saran wrap on top (it's easier to work with)
5. The pie dough will make two crusts.  Roll apx. 1/4 inch think - place in 9" pie plate.  Add filling (see recipe below) 
6. Place cubed butter on top of the fruit filling.  Place top crust on, poke holes with fork (or cut out design).  Crimp edges
7. Place a cookie sheet on the rack below the pie.
8. Use middle rack and bake 10 minutes at 450.  Turn oven down and bake at 350 for  45-50 minutes or until top crust is golden brown.
9. Let cool for 20+ minutes.  Serve with ice cream (ala mode), whipping cream, cheddar... whatever suits your fancy.

---

# Apple pie filling recipe

## Ingredients

* 4-6 medium tart apples (granny smith, honeycrisp, etc.)
* 1C sugar
* 1/4C Flour
* 1tsp Cinnamon
* 1/4tsp Nutmeg
* 2tsp lemon juice and 1/4 (ish)C cold water

1.  Peel, core, and thinly slice apples
2.  Place apples in the lemon water until all are prepped
3.  Dump water, add sugar, flour, and spices.  Mix thoroughly
4.  Add to pie crust and compelete aforementioned directions