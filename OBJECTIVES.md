Objectives
==========

1. GitLab is to GitHub and BitBucket as Linux is to Windows and macOS
2. GitLab has Issues and Boards
3. GitLab has CI
- Track an Issue from Idea to Production
- Create a standalone GitLab server
- Understand how roles affect repo permissions
- Authorize an ssh key and use the git CLI
