# Overview and Setup

Duration: 60 minutes

- SFS Rap
- Sign-ins: Mattermost and sign-in form

## Talk

(15 minutes)

GitLab is a way for individuals and teams to manage code-centric products on a per-project basis. GitLab has git, Issues, Issue Boards, CI/CD, wiki, branch protection, Merge Requests, forking, and it's Open Core.

GitLab vs GitHub, BitBucket, etc... Discussion

GitLab is "open core", like Puppet is and Ansible was (Ansible is now fully free (libre) software). The base product is free software. Some advanced features have a more proprietary license.

GitLab.com has private projects for free, and bronze, silver, and gold subscriptions with additional features.

GitHub.com is proprietary, but seems flatter and more social. Also, way more popular. Also, owned by Microsoft.

GitLab, GitHub, BitBucket all can be self-hosted. GitLab has Community, Starter, Premium, and Ultimate with features interesting to authors, managers, directors, and C-levels, respecitvely. https://about.gitlab.com/2018/11/09/monetizing-and-being-open-source/

## Demo

(15 minutes)

- Mattermost and class sign-in
- Ensure GitLab.com account
- Ensure authorized ssh key
- Create a personal project
  * Create a README.md
  * Modify the README in GUI
  * Modify the README in CLI

## Pair & Share

(30 minutes)

- Mattermost and class sign-in
- Ensure [GitLab.com](https://gitlab.com/) account
- Ensure [authorized ssh key/s](https://gitlab.com/profile/keys)
- Create a [new personal project](https://gitlab.com/projects/new) named 'junque'
  * Create a README.md
  * Modify the README in the web GUI
  * Modify the README in CLI

```bash
git clone git clone git@gitlab.com:${USER}/junque.git
cd junque/
```

1. Type the name of your favorite editor: `atom`, `vim`, `emacs`, `nano`, or whatever.
*  Type <Space>RE<Tab>, the line should auto-complete to README.md
*  Make some changes. Save and exit the editor. 'ZZ' is best in vi/m.

```bash
git status
git add README.md
git status
git commit -m 'update README.md' # or better commit message
git status
git push
git status
```

Raise both hands in the air and make a victorious noise: Woo! or similar.

Teacher: When most pairs have finished, if there's time, lead a group Share.
